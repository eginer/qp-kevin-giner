program test_cisd_e_pos
 implicit none
 read_wf = .True.
 touch read_wf 
 call print_fock_diag
 call routine_compute_cisd 
 if(N_states .ne. 1)then
!  call routine
  call print_wf_exc
 else
  call routine_print_wf
 endif
! call routine
end

subroutine print_fock_diag
 implicit none
 integer :: i
 print*,'electronic/positronic diag Fock elements'
 do i = 1, mo_num
  print*, i,Fock_matrix_diag_mo_e_w_pos(i),Fock_matrix_mo_pos(i,i)
 enddo
end

subroutine routine_compute_cisd 
 implicit none
 ! by providing the provider "cisd_e_pos_energies" you compute the cisd energies 
 ! see the provider "cisd_e_pos_energies" for more information 
 provide cisd_e_pos_energies
 integer :: sze
 sze = n_det_total_e_pos
 ! saves in the ezfio the cisd electronic/positronic wave function 
 ! the information are stored in dets_e_pos/pos_occ, dets_e_pos/N_e_pos_det, etc ... see the routine for more information
 print*,'Reference energy    = ',diag_h_mat_dets_e_pos(1)
 print*,'Decomposition of the correlation energy'
 print*,'e_corr_tot_in_e_pos = ',e_corr_tot_in_e_pos  
 print*,'e_corr_e_e_in_e_pos = ',e_corr_e_e_in_e_pos
 print*,'e_corr_e_p_in_e_pos = ',e_corr_e_p_in_e_pos
 print*,'Total energy comparison '
 print*,'cisd_e_pos_energies(1)',cisd_e_pos_energies(1)
 print*,'e_tot_from_e_corr     ',e_tot_from_e_corr
 call save_wavefunction_general_e_pos(sze,N_states,psi_total_e_pos,sze,cisd_e_pos_coef,occ_total_e_pos)
end

subroutine routine_print_wf
 implicit none
 integer :: i,indx,occ
 double precision :: accu,h0i,accu_tot,contrib,thr,hii,h00,accu_single_e_pos,accu_single_e,accu_double
 thr = 1.d-03
 i= 1
 occ = 1
 call i_H_j_e_pos(psi_total_e_pos(1,1,i),psi_total_e_pos(1,1,i),occ, occ, N_int,h00)
 print*,'h00 = ',h00
 
 print*,'single e pos '
 accu_single_e_pos = 0.d0
 do i = 1, n_single_e_plus
  indx = list_single_e_plus_cisd(i)
  occ = occ_HF_e_single_e_plus(i)
  call i_H_j_e_pos(psi_HF_e_single_e_plus(1,1,1),psi_total_e_pos(1,1,1),occ, 1, N_int,h0i)
  call i_H_j_e_pos(psi_HF_e_single_e_plus(1,1,i),psi_HF_e_single_e_plus(1,1,i),occ, occ, N_int,hii)
  contrib = cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1)*h0i
  accu_single_e_pos += contrib
  if(dabs(contrib).lt.thr)cycle
  print*,'i,occ_pos',i,occ
  call debug_det(psi_HF_e_single_e_plus(1,1,i),N_int)
  print*,cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1),h0i,contrib
 enddo
 print*,'accu_single_e_pos = ',accu_single_e_pos

 accu_single_e = 0.d0
 do i = 1, n_singles_alpha
  indx = list_singles_alpha(i)
  occ = 1
  call i_H_j_e_pos(psi_det(1,1,indx),psi_total_e_pos(1,1,1),occ, 1, N_int,h0i)
  call i_H_j_e_pos(psi_det(1,1,indx),psi_det(1,1,indx),occ, occ, N_int,hii)
  contrib = cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1)*h0i
  accu_single_e += contrib
  if(dabs(contrib).lt.thr)cycle
  print*,'i,occ_pos',i,occ
  call debug_det(psi_det(1,1,indx),N_int)
  print*,cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1),h0i,contrib
 enddo
 do i = 1, n_singles_beta
  indx = list_singles_beta(i)
  occ = 1
  call i_H_j_e_pos(psi_det(1,1,indx),psi_total_e_pos(1,1,1),occ, 1, N_int,h0i)
  call i_H_j_e_pos(psi_det(1,1,indx),psi_det(1,1,indx),occ, occ, N_int,hii)
  contrib = cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1)*h0i
  accu_single_e += contrib
  if(dabs(contrib).lt.thr)cycle
  print*,'i,occ_pos',i,occ
  call debug_det(psi_det(1,1,indx),N_int)
  print*,cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1),h0i,contrib
 enddo
 print*,'accu_single_e = ',accu_single_e

 print*,'double alpha'
 accu_double = 0.d0
 accu = 0.d0
 do i = 1,n_s_alpha_n_s_p
  indx = list_single_e_plus_single_alpha_cisd(i)
  occ = occ_s_alpha_s_e_plus(i)
  call i_H_j_e_pos(psi_s_alpha_s_e_plus(1,1,i),psi_total_e_pos(1,1,1),occ, 1, N_int,h0i)
  call i_H_j_e_pos(psi_s_alpha_s_e_plus(1,1,i),psi_s_alpha_s_e_plus(1,1,i),occ, occ, N_int,hii)
  contrib = cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1)*h0i
  accu += contrib
  if(dabs(contrib).lt.thr)cycle
  print*,'i,occ_pos',i,occ
  call debug_det(psi_s_alpha_s_e_plus(1,1,i),N_int)
  print*,cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1),h0i,contrib
 enddo
 print*,'accu alpha = ',accu
 accu_double = accu
 accu_tot = accu
 accu = 0.d0
 print*,'double beta'
 do i = 1,n_s_beta_n_s_p
  indx = list_single_e_plus_single_beta_cisd(i)
  occ = occ_s_beta_s_e_plus(i)
  call i_H_j_e_pos(psi_s_beta_s_e_plus(1,1,i),psi_total_e_pos(1,1,1),occ, 1, N_int,h0i)
  contrib = cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1)*h0i
  accu += contrib
  if(dabs(contrib).lt.thr)cycle
!  if(dabs(h0i).lt.thr)cycle
  print*,'i,occ_pos',i,occ_s_beta_s_e_plus(i)
  call debug_det(psi_s_beta_s_e_plus(1,1,i),N_int)
  print*,cisd_e_pos_coef(indx,1)/cisd_e_pos_coef(1,1),h0i,contrib
 enddo
 accu_double += accu
 print*,'accu beta  = ',accu
 print*,'-----------'
 print*,'accu_single_e_pos = ',accu_single_e_pos
 print*,'accu_single_e     = ',accu_single_e
 print*,'accu_double       = ',accu_double
 accu_tot = accu_single_e + accu_single_e_pos + accu_double
 print*,'accu tot          = ',accu_tot
 print*,'h00 = ',h00
 print*,'h00 + accu_tot = ',h00+accu_tot
end

subroutine routine
 implicit none
 integer :: i
  print*,'i_orb_exc = ',i_orb_exc
  print*,'index good det ',index_exc_det
  print*,'isorted(i),sorted_coef(i), energy_sorted_exc(i)  '
 do i = 1, N_states
  print*,sorted_istate(i),sorted_coef(i), energy_sorted_exc(i) 
 enddo

 print*,'Wave function'
 do i = 1, index_exc_det
  print*,'-------'
  print*,'i,occ_e_pos',i,occ_total_e_pos(i)
  call debug_det(psi_total_e_pos(1,1,i), N_int)
  write(*,'(100(F10.6,X))')cisd_e_pos_coef(i,:)
 enddo

end
