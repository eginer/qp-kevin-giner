 BEGIN_PROVIDER [ double precision, cisd_e_pos_coef, (n_det_total_e_pos,N_states) ]
&BEGIN_PROVIDER [ double precision, cisd_e_pos_energies, (N_states) ]
 implicit none
 BEGIN_DOC
  ! coefficients and variational energy of the CISD electronic/positronic wave function 
  !
  ! uses the list of electronic Slater determinants of psi_total_e_pos 
  !
  ! and the occupation of the positron of occ_total_e_pos
  !
 END_DOC
 integer :: sze,i,istate
 sze = n_det_total_e_pos
 print*,'coucou ',n_det_total_e_pos
 if(n_det_total_e_pos .lt. n_det_max_full)then ! n_det_max_full is the limit between Lapack and Davidson diagonalization
  print*,'Diagonalizing the CISD electronic/positronic matrix using Lapack'
  double precision, allocatable :: hmatrix(:,:), eigenvectors(:,:), eigenvalues(:)
  allocate(eigenvectors(sze,sze), eigenvalues(sze))
  call lapack_diag(eigenvalues,eigenvectors,H_matrix_all_dets_e_pos,sze,sze)
  do istate = 1, N_states
   cisd_e_pos_energies(istate) = eigenvalues(istate)
   do i = 1, sze
    cisd_e_pos_coef(i,istate) = eigenvectors(i,istate) 
   enddo
  enddo
 else
  print*,'Minimizing the energy of the CISD electronic/positronic wave function with Davidson'
  double precision, allocatable :: u_in(:,:), energies(:), h_jj(:)
  logical :: converged
  allocate(u_in(sze,N_states_diag),energies(N_states_diag),h_jj(sze))
  u_in = 0.d0
  do istate = 1, N_states
   do i = 1, N_det
!    u_in(i,istate) = psi_coef(i,istate)
  !! Create a guess for the positronic/electronic wave function 
  !! Takes the simplest possible
    u_in(i,istate) = 1.d0
   enddo
  enddo
  ! hpsi_e_pos :: routine that apply H on a wave function corresponding to psi_total_e_pos
  ! needed for the Davidson 
  external :: hpsi_e_pos 
  ! General Davidson routine taking :: 
  !    +)   "u_in" as a GUESS of the lowest eigenvectors of the Hamiltonian 
  !    +)   "diag_H_mat_dets_e_pos" which are diagonal matrix elements  
  !    +)   "sze" which is the size of the vectors 
  !    +)   "N_states" which is the number of states that you are looking for 
  !    +)   "N_states_diag" maximum number of application of H for the Krylov space 
  !    +)   "hpsi_e_pos" is an EXTERNAL SUBROUTINE that applies the Hamiltonian to the vector 
  !    +)   for more information see in dav_general_mat/test_dav.irp.f 
  converged = .False.
  integer :: iit
  iit = 0
  do while (.not.converged)
   call davidson_general_ext_rout(u_in,diag_H_mat_dets_e_pos,energies,sze,N_states,N_states_diag,converged,hpsi_e_pos)
   iit += 1
   if(iit.gt.10*n_states)then
    print*,'WARNING !!!'
    print*,'Did not converge after ',iit,'iterations'
    print*,'The highest eigenvectors might not be good ...'
    print*,'exiting loop'
    exit
   endif
  enddo
  double precision, allocatable :: dress_jj(:)
  double precision :: ecorr,h00,ebefore,eafter,thresh
  if(cepa_zero)then
   integer :: it
   it = 0
   thresh = 1.d-6
   allocate(dress_jj(sze))
   converged = .False.
   dress_jj(1) = 0.d0
   call i_H_j_e_pos(psi_total_e_pos(1,1,1),psi_total_e_pos(1,1,1),1, 1, N_int,h00)
   ebefore = energies(1)
   print*,'Doing CEPA0 method'
   do while(.not.converged)
    it += 1
    ecorr = energies(1) - h00
    print*,'------------------'
    print*,'it = ',it
    print*,'ecorr = ',ecorr
    do i = 2, sze
     if(diag_H_mat_dets_e_pos(i)+ecorr.gt.h00)then
      dress_jj(i) = ecorr
     endif
    enddo
    call davidson_general_ext_rout_diag_dressed(u_in,diag_H_mat_dets_e_pos,Dress_jj,energies,sze,N_states,N_states_diag,converged,hpsi_e_pos)
    eafter = energies(1)
    converged = dabs(ebefore-eafter).lt.thresh
    ebefore = eafter
   enddo
  endif
  ! the Davidson routine returns :: 
  !    +)   the lowest N_states eigenvectors in "u_in" 
  !    +)   and the corresponding variational energies in "energies"
  do istate = 1, N_states
   cisd_e_pos_energies(istate) = energies(istate)
   do i = 1, sze
    cisd_e_pos_coef(i,istate) = u_in(i,istate)
   enddo
  enddo
 endif

 print*,'Enegies for the lowest states '
 do istate = 1, N_states
  print*,'istate = ',istate
  print*,'<Psi_e/pos | H | Psi_e/pos> = ',cisd_e_pos_energies(istate)
 enddo
END_PROVIDER 


 BEGIN_PROVIDER [ double precision, e_corr_e_e_in_e_pos ]
&BEGIN_PROVIDER [ double precision, e_corr_e_p_in_e_pos ]
&BEGIN_PROVIDER [ double precision, e_corr_tot_in_e_pos ]
&BEGIN_PROVIDER [ double precision, e_tot_from_e_corr ]
 implicit none
 integer :: j,j_pos,i_ref,i_ref_pos
 double precision :: hij,coef
 e_corr_e_e_in_e_pos = 0.d0
 e_corr_e_p_in_e_pos = 0.d0
 ! Purely electronic excitations 
 i_ref_pos = occ_total_e_pos(1) 
 do j = 2, N_det
  j_pos = occ_total_e_pos(j) 
  ! <HF(elec) * HF(p+) | H | HF(p+) * Exc(elec) >
  call i_H_j_e_pos(psi_total_e_pos(1,1,1),psi_total_e_pos(1,1,j),i_ref_pos, j_pos, N_int,hij)
  e_corr_e_e_in_e_pos += hij * cisd_e_pos_coef(j,1)/cisd_e_pos_coef(1,1)
 enddo
 ! Purely positronic and/or mixed electronic/positronic excitation 
 do j = N_det+1, n_det_total_e_pos
  j_pos = occ_total_e_pos(j) 
  ! <HF(elec) * HF(p+) | H | Exc(p+) * Exc(elec) >
  call i_H_j_e_pos(psi_total_e_pos(1,1,1),psi_total_e_pos(1,1,j),i_ref_pos, j_pos, N_int,hij)
  e_corr_e_p_in_e_pos += hij * cisd_e_pos_coef(j,1)/cisd_e_pos_coef(1,1)
 enddo
 call i_H_j_e_pos(psi_total_e_pos(1,1,1),psi_total_e_pos(1,1,1),i_ref_pos, i_ref_pos, N_int,hij)
 e_tot_from_e_corr = hij + e_corr_e_p_in_e_pos + e_corr_e_e_in_e_pos
 e_corr_tot_in_e_pos  = e_corr_e_p_in_e_pos + e_corr_e_e_in_e_pos

END_PROVIDER 
