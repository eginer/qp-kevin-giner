subroutine get_cm_energy_for_e
 implicit none
 integer :: i,j
 integer :: i_pos,j_pos
 i_pos = 1
 j_pos = 1
 double precision :: o1_ij, o2_ij,accu1,accu2,norm
 double precision, allocatable :: kin_one_half(:,:),pos_e_int(:,:,:,:)
 allocate(kin_one_half(mo_num, mo_num),pos_e_int(mo_num, mo_num, mo_num, mo_num))
 kin_one_half = 0.5d0 * mo_kinetic_integrals
 pos_e_int = 0.d0

 accu1 = 0.d0
 accu2 = 0.d0
 norm  = 0.D0
 do i = 1, N_det
  norm += psi_coef(i,1)*psi_coef(i,1)
  do j = 1, N_det
   call i_o1_o2_j(psi_det(1,1,i), psi_det(1,1,j), i_pos, j_pos, &
    kin_one_half, pos_e_int, grad_scal_ints, pos_e_int, N_int, o1_ij, o2_ij)
   accu1 += o1_ij * psi_coef(i,1) * psi_coef(j,1)
   accu2 += o2_ij * psi_coef(i,1) * psi_coef(j,1)
  enddo
 enddo
 norm = dsqrt(norm)
 accu1 = accu1/norm
 accu2 = accu2/norm
 print*,'<o1> = ',accu1
 print*,'<o2> = ',accu2
 print*,'sum  = ',accu1 + accu2
 call print_energy_components()

end

subroutine get_cm_energy_for_e_pos
 implicit none
 integer :: i,j
 integer :: i_pos,j_pos
 double precision :: o1_ij, o2_ij,accu1,accu2,norm
 double precision, allocatable :: kin_one_half_e(:,:),kin_one_half_pos(:,:)
 allocate(kin_one_half_e(mo_num, mo_num),kin_one_half_pos(mo_num, mo_num))
 kin_one_half_e   = 0.5d0 * mo_kinetic_integrals
 kin_one_half_pos = 0.5d0 * mo_pos_kinetic_integrals
 print*,'Computing the center of mass energy '
 accu1 = 0.d0
 accu2 = 0.d0
 norm  = 0.D0
 do i = 1, N_e_pos_det
  norm += psi_e_pos_coef(i,1) * psi_e_pos_coef(i,1)
  i_pos = pos_occ(i)
  do j = 1, N_e_pos_det
   j_pos = pos_occ(j)
   call i_o1_o2_j(psi_e_pos_det(1,1,i), psi_e_pos_det(1,1,j), i_pos, j_pos, &
    kin_one_half_e, kin_one_half_pos, grad_scal_ints, e_pos_grad_scal_ints, N_int, o1_ij, o2_ij)
    ! kin for e,     kin for pos      p1.p2 for elec   p1 . p2 for e-pos 
   accu1 += o1_ij * psi_e_pos_coef(i,1) * psi_e_pos_coef(j,1)
   accu2 += o2_ij * psi_e_pos_coef(i,1) * psi_e_pos_coef(j,1)
  enddo
 enddo
 norm = dsqrt(norm)
 accu1 = accu1/norm
 accu2 = accu2/norm
 print*,'<o1> = ',accu1
 print*,'<o2> = ',accu2
 print*,'sum  = ',accu1 + accu2
! call print_energy_components()

end

subroutine get_relative_energy_for_e_pos
 implicit none
 integer :: i,j
 integer :: i_pos,j_pos
 double precision :: o1_ij, o2_ij,accu1,accu2,norm
 double precision, allocatable :: kin_one_half_e(:,:),kin_one_half_pos(:,:)
 allocate(kin_one_half_e(mo_num, mo_num),kin_one_half_pos(mo_num, mo_num))
 kin_one_half_e   = 0.5d0 * mo_kinetic_integrals
 kin_one_half_pos = 0.5d0 * mo_pos_kinetic_integrals
 print*,'Computing the relative  energy '
 accu1 = 0.d0
 accu2 = 0.d0
 norm  = 0.D0
 do i = 1, N_e_pos_det
  norm += psi_e_pos_coef(i,1) * psi_e_pos_coef(i,1)
  i_pos = pos_occ(i)
  do j = 1, N_e_pos_det
   j_pos = pos_occ(j)
   call i_o1_o2_j(psi_e_pos_det(1,1,i), psi_e_pos_det(1,1,j), i_pos, j_pos, &
    kin_one_half_e, kin_one_half_pos, grad_scal_ints, minus_e_pos_grad_scal_ints, N_int, o1_ij, o2_ij)
    ! kin for e,     kin for pos      p1.p2 for elec   p1 . p2 for e-pos 
   accu1 += o1_ij * psi_e_pos_coef(i,1) * psi_e_pos_coef(j,1)
   accu2 += o2_ij * psi_e_pos_coef(i,1) * psi_e_pos_coef(j,1)
  enddo
 enddo
 norm = dsqrt(norm)
 accu1 = accu1/norm
 accu2 = accu2/norm
 print*,'<o1> = ',accu1
 print*,'<o2> = ',accu2
 print*,'sum  = ',accu1 + accu2
! call print_energy_components()

end


subroutine get_total_kinetic_contrib
 implicit none
 integer :: i,j
 integer :: i_pos,j_pos
 double precision :: o1_ij, accu1,norm
 print*,'Computing the center of mass energy '
 accu1 = 0.d0
 norm  = 0.D0
 do i = 1, N_e_pos_det
  norm += psi_e_pos_coef(i,1) * psi_e_pos_coef(i,1)
  i_pos = pos_occ(i)
  do j = 1, N_e_pos_det
   j_pos = pos_occ(j)
!   call i_o1_j_pos(psi_e_pos_det(1,1,i), psi_e_pos_det(1,1,j), i_pos, j_pos,mo_kinetic_integrals,mo_pos_kinetic_integrals,N_int,o1_ij)
   call i_o1_j_pos(psi_e_pos_det(1,1,i), psi_e_pos_det(1,1,j), i_pos, j_pos,mo_one_e_integrals,mo_pos_integrals,N_int,o1_ij)
   accu1 += o1_ij * psi_e_pos_coef(i,1) * psi_e_pos_coef(j,1)
  enddo
 enddo
 norm = dsqrt(norm)
 accu1 = accu1/norm
 print*,'< T >= ',accu1
! call print_energy_components()

end


subroutine get_var_e_pos_energy
 implicit none
  use bitmasks
 integer :: i,j
 integer :: i_pos, j_pos
 double precision :: accu, norm, o1_ij, o2_ij, hij,accu2,accu1
 norm = 0.d0
 accu = 0.d0
 accu2 = 0.d0
 accu1 = 0.d0
 do i = 1, N_e_pos_det
  norm += psi_e_pos_coef(i,1) * psi_e_pos_coef(i,1)
  i_pos = pos_occ(i)
  do j = 1, N_e_pos_det
   j_pos = pos_occ(j)
   call i_o1_o2_j(psi_e_pos_det(1,1,i), psi_e_pos_det(1,1,j), i_pos, j_pos, &
    mo_one_e_integrals , mo_pos_integrals, two_e_ints_array, mo_two_e_pos_int, N_int, o1_ij, o2_ij)
   accu1 += o1_ij * psi_e_pos_coef(i,1) * psi_e_pos_coef(j,1)
   accu2 += o2_ij * psi_e_pos_coef(i,1) * psi_e_pos_coef(j,1)
   call  i_H_j_e_pos(psi_e_pos_det(1,1,i), psi_e_pos_det(1,1,j),i_pos, j_pos, N_int,hij)
   if(dabs(o1_ij + o2_ij - hij).gt.1.d-10)then
    print*,i,j
    print*,o1_ij + o2_ij,  hij, dabs(o1_ij + o2_ij - hij)
    integer :: degree
    print*,'i_pos,j_pos',i_pos,j_pos
    call get_excitation_degree(psi_e_pos_det(1,1,i), psi_e_pos_det(1,1,j),degree,N_int)
    print*,'degree= ',degree
    stop
   endif
   accu  += ( hij ) * psi_e_pos_coef(i,1) * psi_e_pos_coef(j,1)
  enddo
 enddo
 norm = dsqrt(norm)
 print*,'accu = ',accu/norm
 print*,'<o1> = ',accu1/norm
 print*,'<o2> = ',accu2/norm
 print*,'sum  = ',(accu1+accu2)/norm
end
