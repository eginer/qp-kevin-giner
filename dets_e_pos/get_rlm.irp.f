subroutine get_rlm(radius,function_to_get,l,rlm)
 implicit none
 double precision, intent(in)  :: radius
 integer, intent(in)           :: l
 integer                       :: j,m
 double precision, intent(out) :: rlm(2,-l:l)
 double precision, allocatable :: weights(:),grid_points_on_sphere(:,:)
 double precision              :: center(3),r(3),function_at_r,re_ylm_1, im_ylm_1,weight,accu_weight
 external function_to_get
 allocate(grid_points_on_sphere(3,n_points_integration_angular),weights(n_points_integration_angular))
 center=0.d0
 call create_spherical_grids_for_r(radius, center, weights, grid_points_on_sphere) 
 accu_weight=0.d0
 rlm=0.d0
 do j=1,n_points_integration_angular
   weight=weights(j)  
   r(1:3)=grid_points_on_sphere(1:3,j)
   call function_to_get(r,function_at_r)
   accu_weight+=weight
   do m=-l,l
     call spher_harm_func_r3(r,l,m,re_ylm_1, im_ylm_1)
     rlm(1,m)+=weight*function_at_r*re_ylm_1
     rlm(2,m)-=weight*function_at_r*im_ylm_1
   end do
 end do
 rlm*=1.d0/radius**2
end subroutine get_rlm
