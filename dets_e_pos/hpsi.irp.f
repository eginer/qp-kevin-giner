subroutine hpsi_e_pos(v,u,N_st,sze)
  use bitmasks
  implicit none
  BEGIN_DOC
  !
  ! Computes $v = H | u \rangle$ 
  !
  ! where H = Hee + Hpp + Hep
  !
  ! here "u" and "v" must be coherent with the list of slater determinants psi_total_e_pos
  !
  ! so size(u,1) = n_det_total_e_pos and size(v,1) = n_det_total_e_pos
  !
  ! the application of the "purely electronic part" is done through H_u_0_e_pos_nstates_openmp 
  !
  ! which assumes that the N_det first determinants of psi_total_e_pos are those of psi_det 
  END_DOC
  integer, intent(in)              :: N_st,sze
  double precision, intent(in)     :: u(sze,N_st)
  double precision, intent(inout)  :: v(sze,N_st)
  double precision                 :: hij
  integer                          :: i,j,istate,i_pos,j_pos
  integer                          :: degree_i,degree_j,spin_exc_i(2), spin_exc_j(2)
  integer                          :: hp_i(4), hp_j(4)
  logical                          :: compute
!
!   The electronic/positronic wave function is as follows 
!   CISD * pos(phi_0) |  HF * S(pos) , S(elec alpha/beta) * S(pos) 
!   Here S(pos) means "single excitation of the positron with respect to its ground state" 
!        S(elec alpha/bet) means "single excitation of the alpha/beta electron with respect to HF"
!   The Hamiltonian matrix can be split into different group
!  
!                    |CISD * pos(phi_0)== N_det   |  HF * S(pos) , S(elec alpha/beta) * S(pos) 
!   -----------------|-------------------------------------------------------------------------
!   CISD * pos(phi_0)| <I_elec|H^{elec}|J_elec>   | <I_elec,phi_0|H^elec/pos |J_elec,S(pos)>
!                    |+<I_elec|H^{e/e+}|J_elec>   |                                             
!   -----------------|------------------------------------------------------------------------
!    S(pos) *        |                            | <I_elec,S'(pos)|H^elec/pos |J_elec,S(pos)>   
!    HF / S(elec a/b)|                            |                                            
!                    |                            | 
!
!   Compute the purely electronic part of the Hamiltonian matrix element
!   It uses psi_det as electronic wave function 
!   Therefore the electronic/positronic wave function with the positron in its ground state 
!   must have the same determinants and in the same order than psi_det 
!
!   TODO ::: if N_st, better to transpose the u and v maybe 
!   TODO ::: parallelize and improve the application of H that does not concer the bloc <I_elec|H^{elec}|J_elec>
!
 double precision, allocatable :: cmax(:)
 allocate(cmax(sze))
 cmax = 0.d0
 do istate = 1, N_st
  do i = 1, sze
   cmax(i) += u(i,istate)*u(i,istate)
  enddo
 enddo
 do i = 1, sze
  cmax(i) = dsqrt(cmax(i))/dble(N_st)
 enddo
  v = 0.d0
  !Computes the general <I_elec,phi_0| H^elec/pos |J_elec,phi_0)> 
  !This is done through a no_very efficient routine 
  double precision, allocatable :: u_tmp(:,:), v_tmp(:,:)
  double precision :: wall1, wall2,thr
  thr = threshold_coef_hpsi
  call wall_time(wall1)
  allocate(u_tmp(N_det,N_st),v_tmp(N_det,N_st))
  u_tmp(1:N_det,1:N_st) = u(1:N_det,1:N_st)
  v_tmp = 0.d0
  !subroutine that assumes that v_tmp and u_tmp are of size N_det 
  call H_u_0_e_pos_nstates_openmp(v_tmp,u_tmp,N_st,N_det) 
  do j = 1, N_st
   do i = 1, N_det
    v(i,j) = v_tmp(i,j)
   enddo
  enddo

  call wall_time(wall2)
  !write(6,*) 'CISD for e- [s]',wall2 - wall1
  call wall_time(wall1)

  !Computes the general <I_elec,phi_0| H^elec/pos |J_elec,S(pos)>
  !$OMP PARALLEL                  &
  !$OMP DEFAULT (NONE)            &
  !$OMP PRIVATE (i,j,i_pos,j_pos,hij,istate) &
  !$OMP PRIVATE (degree_i,degree_j,spin_exc_i,spin_exc_j,hp_i,hp_j,compute) & 
  !$OMP SHARED (occ_total_e_pos,N_det,n_det_total_e_pos,psi_total_e_pos,N_int,N_st,v,u,thr,cmax) & 
  !$OMP SHARED (degree_exc_total_e_pos,spin_exc_total_e_pos,hp_total_e_pos,optimisation)
  !$OMP DO SCHEDULE (guided) 
  !COLLAPSE(2)
  do i = 1, N_det
    i_pos = occ_total_e_pos(i) 
    degree_i = degree_exc_total_e_pos(i)
    spin_exc_i(:) = spin_exc_total_e_pos(:,i)
    hp_i(:) = hp_total_e_pos(:,i)
    do j = N_det+1, n_det_total_e_pos
      if (dabs(cmax(j)).lt.thr) cycle
      j_pos = occ_total_e_pos(j) 
      degree_j = degree_exc_total_e_pos(j)
      spin_exc_j(:) = spin_exc_total_e_pos(:,j)
      hp_j(:) = hp_total_e_pos(:,j)
      !OPTIMISATION 
      if (optimisation.eqv..true.) then 
        call slater_rules_improve(degree_i,degree_j,spin_exc_i,spin_exc_j,hp_i,hp_j,i_pos,j_pos,compute)
        if (compute.eqv..false.) cycle
      end if  
      call i_H_j_e_pos(psi_total_e_pos(1,1,i),psi_total_e_pos(1,1,j),i_pos, j_pos, N_int,hij)
      do istate = 1, N_st
        v(i,istate) += hij * u(j,istate) 
      end do
    end do
  end do
  !$OMP END DO
  !$OMP END PARALLEL

  !Computes the general <J_elec,S(pos)| H^elec/pos |I_elec,phi_0>
  !$OMP PARALLEL                  &
  !$OMP DEFAULT (NONE)            &
  !$OMP PRIVATE (i,j,i_pos,j_pos,hij,istate) &
  !$OMP PRIVATE (degree_i,degree_j,spin_exc_i,spin_exc_j,hp_i,hp_j,compute) & 
  !$OMP SHARED (occ_total_e_pos,N_det,n_det_total_e_pos,psi_total_e_pos,N_int,N_st,v,u,thr,cmax) & 
  !$OMP SHARED (degree_exc_total_e_pos,spin_exc_total_e_pos,hp_total_e_pos,optimisation)
  !$OMP DO SCHEDULE (guided) 
  !COLLAPSE(2)
  do i = N_det+1, n_det_total_e_pos
    i_pos = occ_total_e_pos(i) 
    degree_i = degree_exc_total_e_pos(i)
    spin_exc_i(:) = spin_exc_total_e_pos(:,i)
    hp_i(:) = hp_total_e_pos(:,i)
    do j = 1, N_det
      if (dabs(cmax(j)).lt.thr) cycle
      j_pos = occ_total_e_pos(j) 
      degree_j = degree_exc_total_e_pos(j)
      spin_exc_j(:) = spin_exc_total_e_pos(:,j)
      hp_j(:) = hp_total_e_pos(:,j)
      !OPTIMISATION 
      if (optimisation.eqv..true.) then
        call slater_rules_improve(degree_i,degree_j,spin_exc_i,spin_exc_j,hp_i,hp_j,i_pos,j_pos,compute)
        if (compute.eqv..false.) cycle
      end if  
      call i_H_j_e_pos(psi_total_e_pos(1,1,i),psi_total_e_pos(1,1,j),i_pos, j_pos, N_int,hij)
      do istate = 1, N_st
        v(i,istate) += hij * u(j,istate) 
      end do
    end do
  end do
  !$OMP END DO
  !$OMP END PARALLEL
 

  !Computes the general <J_elec,S(pos)|H^elec/pos |J_elec,S(pos)>
  !$OMP PARALLEL                  &
  !$OMP DEFAULT (NONE)            &
  !$OMP PRIVATE (i,j,i_pos,j_pos,hij,istate) & 
  !$OMP SHARED (occ_total_e_pos,N_det,n_det_total_e_pos,psi_total_e_pos,N_int,N_st,v,u,thr,cmax)
  !$OMP DO SCHEDULE (guided) 
  !COLLAPSE(2)
  do i = N_det+1, n_det_total_e_pos
   i_pos = occ_total_e_pos(i) 
   do j = N_det+1, n_det_total_e_pos
    j_pos = occ_total_e_pos(j) 
    if(dabs(cmax(j)).lt.thr)cycle
     call i_H_j_e_pos(psi_total_e_pos(1,1,i),psi_total_e_pos(1,1,j),i_pos, j_pos, N_int,hij)
     do istate = 1, N_st
      v(i,istate) += hij * u(j,istate) 
     enddo
   enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  call wall_time(wall2)
  !write(6,*) 'CISD for e-|e+ [s]',wall2 - wall1
end subroutine hpsi_e_pos

subroutine test_h_psi_e_pos
 BEGIN_DOC
 !routine that test the routines h_psi_e_pos that is usefull for Davidson
 END_DOC
 implicit none
 double precision, allocatable :: hmatrix(:,:), v0(:), v0_bis(:), u0(:)
 integer :: i,j,sze,n_st
 sze = n_det_total_e_pos
 n_st = 1
 allocate(u0(sze),v0(sze),v0_bis(sze))
 !! creates a stupid vector in u0
 do i = 1, N_det
  u0(i) = 1.d0
 enddo
 !Applies the Hamiltonian matrix to u0 :: v0 = H u0
 v0 = 0.d0
 do i = 1, N_det
  do j = 1, N_det
   v0(i) += u0(j) * H_matrix_all_dets_e_pos(j,i) 
  enddo
 enddo
 print*,'Testing the h_psi_e_pos routine '
 !! v0_bis = H u0
 call hpsi_e_pos(v0_bis,u0,n_st,sze)
 double precision :: accu
 accu = 0.d0
 !compares the exact answer v0 with the new answer v0_bis 
 do i = 1, n_det
  accu += dabs(v0(i) - v0_bis(i))
 enddo
 print*,'accu = ',accu
 print*,'In theory accu must be "zero" (in the numerical sense)'
end subroutine test_h_psi_e_pos

subroutine slater_rules_improve(degree_i,degree_j,spin_exc_i,spin_exc_j,hp_i,hp_j,i_pos,j_pos,compute)
 implicit none
 integer, intent(in)  :: degree_i,degree_j,spin_exc_i(2),spin_exc_j(2)
 integer, intent(in)  :: hp_i(4),hp_j(4),i_pos,j_pos
 logical, intent(out) :: compute
 logical              :: cond_1,cond_2
 compute=.true.
 if (j_pos.ne.1) then
   !COLUMN 1
   if (degree_j.eq.0) then
     !LINES 4-6
     if ((degree_i.gt.1).and.(i_pos.eq.1)) then
       compute=.false.
       return
     end if
   !COLUMN 2 OR 3
   else if (degree_j.eq.1) then
     !LINES 2-3
     if ((degree_i.eq.1).and.(i_pos.eq.1)) then
       if (spin_exc_j(1).ne.spin_exc_i(1)) then
         compute=.false.
       else
         !CHECK OF THE MATCHING
         compute=(hp_j(1).eq.hp_i(1)).or.(hp_j(2).eq.hp_i(2))
       end if
     !LINES 4-6  
     else if ((degree_i.eq.2).and.(i_pos.eq.1)) then
       !LINES 4-5
       if (spin_exc_i(1).eq.spin_exc_i(2)) then
         if (spin_exc_j(1).ne.spin_exc_i(1)) then
           compute=.false.
         else
           !ONE MATCHING AT LEAST IS REQUIRED
           cond_1=(hp_j(2).eq.hp_i(2)).or.(hp_j(2).eq.hp_i(4))
           cond_2=(hp_j(1).eq.hp_i(1)).or.(hp_j(1).eq.hp_i(3)) 
           compute=(cond_1.eqv..true.).and.(cond_2.eqv..true.)
         end if
       !LINE 6  
       else   
         !ONE MATCHING AT LEAST IS REQUIRED
         if ((spin_exc_j(1).eq.spin_exc_i(1)).or.(spin_exc_j(1).eq.spin_exc_i(2))) then
           cond_1=(hp_j(1).eq.hp_i(1)).and.(hp_j(2).eq.hp_i(2))
           cond_2=(hp_j(1).eq.hp_i(3)).and.(hp_j(2).eq.hp_i(4))
           compute=(cond_1.eqv..true.).or.(cond_2.eqv..true.)
         end if
       end if
     end if
   end if
 end if   
end subroutine slater_rules_improve
