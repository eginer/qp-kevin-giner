program print_e_exc
 implicit none
  use bitmasks ! you need to include the bitmasks_module.f90 features
  double precision :: hij
 ! <det_i * i_pos | H | det_j * j_pos >
  call i_H_j_e_pos(det_exc_ref,det_exc_ref,1, 1, N_int,hij)
 print*,'hij = ',hij
end
