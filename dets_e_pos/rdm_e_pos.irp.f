BEGIN_PROVIDER [double precision,rdm_elec_in_e_pos,(mo_num, mo_num,2,n_states)]
  implicit none
  integer               :: i,j,i_pos,j_pos,degree,k,p,q
  integer               :: n_occ_ab,exc 
  integer               :: h1,p1,s1,h2,p2,s2
  integer, allocatable  :: occ(:,:)
  double precision      :: c_i,c_j,norm,phase
  dimension n_occ_ab(2),exc(0:2,2,2)
  allocate(occ(N_int*bit_kind_size,2))
  rdm_elec_in_e_pos=0.d0
  do i=1,N_e_pos_det
    i_pos=pos_occ(i)
    do j=1,N_e_pos_det
      j_pos=pos_occ(j)
      if (i_pos.ne.j_pos) cycle
      call get_excitation_degree(psi_e_pos_det(1,1,i),psi_e_pos_det(1,1,j),degree,N_int)
      if (degree.gt.1) cycle
      do k=1,n_states
        c_i=psi_e_pos_coef(i,k) 
        if (k.eq.1) norm+=c_i*c_i 
        c_j=psi_e_pos_coef(j,k)
        !CASE I.EQ.J
        if (degree.eq.0) then
          call bitstring_to_list_ab(psi_e_pos_det(1,1,i),occ,n_occ_ab,N_int)
          do q=1,2
            do p=1,n_occ_ab(q)
              rdm_elec_in_e_pos(occ(p,q),occ(p,q),q,k)+=c_i*c_i   
            end do
          end do
        !CASE I.NE.J  
        else if (degree.eq.1) then
          call get_excitation(psi_e_pos_det(1,1,i),psi_e_pos_det(1,1,j),exc,degree,phase,N_int)
          call decode_exc(exc,degree,h1,p1,h2,p2,s1,s2)
          rdm_elec_in_e_pos(h1,p1,s1,k)+=c_i*c_j*phase
        end if
      end do
    end do
 end do
 deallocate(occ)  
END_PROVIDER

BEGIN_PROVIDER [double precision,rdm_elec_in_e_pos_full_mo,(mo_num,mo_num,n_states)]
  implicit none
  integer :: i,j,k
  rdm_elec_in_e_pos_full_mo=0.d0
  do i=1,mo_num
    do j=1,mo_num
      do k=1,n_states
        rdm_elec_in_e_pos_full_mo(i,j,k)=sum(rdm_elec_in_e_pos(i,j,:,k))
      end do
    end do  
  end do
END_PROVIDER

BEGIN_PROVIDER [double precision,rdm_elec_full_mo,(mo_num,mo_num,n_states)]
  implicit none
  integer :: i,j,k
  rdm_elec_full_mo=0.d0
  do i=1,mo_num
    do j=1,mo_num
      do k=1,n_states
        rdm_elec_full_mo(i,j,k)=one_e_dm_mo_alpha_for_dft(i,j,k)+one_e_dm_mo_beta_for_dft(i,j,k)
      end do
    end do  
  end do
END_PROVIDER

BEGIN_PROVIDER [double precision,rdm_elec_in_e_pos_full_ao,(ao_num,ao_num,n_states)]
  implicit none
  integer                       :: k
  double precision, allocatable :: tab_ao(:,:),tab_mo(:,:)
  allocate(tab_mo(mo_num,mo_num),tab_ao(ao_num,ao_num))
  do k=1,n_states
    tab_mo(:,:)=rdm_elec_in_e_pos_full_mo(:,:,k)
    call mo_to_ao_no_overlap(tab_mo,mo_num,tab_ao,ao_num)
    rdm_elec_in_e_pos_full_ao(:,:,k)=tab_ao(:,:)
  end do
  deallocate(tab_mo,tab_ao)
END_PROVIDER

BEGIN_PROVIDER [double precision,rdm_elec_full_ao,(ao_num,ao_num,n_states)]
  implicit none
  integer                       :: k
  double precision, allocatable :: tab_ao(:,:),tab_mo(:,:)
  allocate(tab_mo(mo_num,mo_num),tab_ao(ao_num,ao_num))
  do k=1,n_states
    tab_mo(:,:)=rdm_elec_full_mo(:,:,k)
    call mo_to_ao_no_overlap(tab_mo,mo_num,tab_ao,ao_num)
    rdm_elec_full_ao(:,:,k)=tab_ao(:,:)
  end do
  deallocate(tab_mo,tab_ao)
END_PROVIDER

BEGIN_PROVIDER [double precision,rdm_pos_in_e_pos_mo,(mo_num,mo_num,n_states)]
  implicit none
  integer               :: i,j,k,i_pos,j_pos,degree
  double precision      :: c_i,c_j
  rdm_pos_in_e_pos_mo=0.d0
  do i=1,N_e_pos_det
    i_pos=pos_occ(i)
    do j=1,N_e_pos_det
      j_pos=pos_occ(j)
      call get_excitation_degree(psi_e_pos_det(1,1,i),psi_e_pos_det(1,1,j),degree,N_int)
      if (degree.ne.0) cycle
      do k=1,n_states
        c_i=psi_e_pos_coef(i,k)
        c_j=psi_e_pos_coef(j,k)
        if (i_pos.eq.j_pos) then
          rdm_pos_in_e_pos_mo(i_pos,i_pos,k)+=c_i*c_i
        else 
          rdm_pos_in_e_pos_mo(i_pos,j_pos,k)+=c_i*c_j
        end if
      end do
    end do
  end do
END_PROVIDER

BEGIN_PROVIDER [double precision,rdm_pos_in_e_pos_ao,(ao_num,ao_num,n_states)]
  implicit none
  integer                       :: k
  double precision, allocatable :: tab_ao(:,:),tab_mo(:,:)
  allocate(tab_mo(mo_num,mo_num),tab_ao(ao_num,ao_num))
  do k=1,n_states
    tab_mo(:,:)=rdm_pos_in_e_pos_mo(:,:,k)
    call mo_to_ao_no_overlap(tab_mo,mo_num,tab_ao,ao_num)
    rdm_pos_in_e_pos_ao(:,:,k)=tab_ao(:,:)
  end do
  deallocate(tab_mo,tab_ao)
END_PROVIDER


BEGIN_PROVIDER [double precision,trace_rdm_pos_in_e_pos,(n_states)]
  implicit none
  integer           :: k,i
  double precision  :: accu
  do k=1,n_states
    accu=0.d0
    do i=1,mo_num
      accu+=rdm_pos_in_e_pos_mo(i,i,k)
    end do
    trace_rdm_pos_in_e_pos(k)=accu
  end do
END_PROVIDER 
 
BEGIN_PROVIDER [double precision,rdm_elec_in_e_pos_eigvectors_sorted,(mo_num,elec_alpha_num,n_states)]
  implicit none
  integer                        :: k,i,j
  double precision, allocatable  :: rdm_eigvectors(:)
  allocate(rdm_eigvectors(mo_num))
  do k=1,n_states
    do i=mo_num-elec_alpha_num+1,mo_num
      rdm_eigvectors(:)=dabs(rdm_elec_in_e_pos_eigvectors(:,i,k))
      do j=1,elec_alpha_num
        if (j.eq.maxloc(rdm_eigvectors,1)) rdm_elec_in_e_pos_eigvectors_sorted(:,j,k)=rdm_elec_in_e_pos_eigvectors(:,i,k)
      end do
    end do
  end do
  deallocate(rdm_eigvectors)
END_PROVIDER
