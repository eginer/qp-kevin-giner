
subroutine read_dets_e_pos(det,Nint,Ndet)
  use bitmasks
  implicit none
  BEGIN_DOC
  ! Reads the dets_e_pos from the |EZFIO| file
  END_DOC

  integer, intent(in)            :: Nint,Ndet
  integer(bit_kind), intent(out) :: det(Nint,2,Ndet)
  integer*8, allocatable         :: psi_det_read(:,:,:)
  double precision, allocatable  :: psi_coef_read(:,:)
  integer*8                      :: det_8(100)
  integer(bit_kind)              :: det_bk((100*8)/bit_kind)
  integer                        :: N_int2
  integer                        :: i,k
  equivalence (det_8, det_bk)

  call ezfio_get_determinants_N_int(N_int2)
  ASSERT (N_int2 == Nint)
  call ezfio_get_determinants_bit_kind(k)
  ASSERT (k == bit_kind)

  N_int2 = (Nint*bit_kind)/8
  allocate (psi_det_read(N_int2,2,Ndet))
  call ezfio_get_dets_e_pos_psi_e_pos_det(psi_det_read)
  do i=1,Ndet
    do k=1,N_int2
      det_8(k) = psi_det_read(k,1,i)
    enddo
    do k=1,Nint
      det(k,1,i) = det_bk(k)
    enddo
    do k=1,N_int2
      det_8(k) = psi_det_read(k,2,i)
    enddo
    do k=1,Nint
      det(k,2,i) = det_bk(k)
    enddo
  enddo
  deallocate(psi_det_read)

end

subroutine save_wavefunction_general_e_pos(ndet,nstates,psidet,dim_psicoef,psicoef,occpos)
  implicit none
  BEGIN_DOC
  !  Save the wave function into the |EZFIO| file
  END_DOC
  use bitmasks
  include 'constants.include.F'
  integer, intent(in)            :: ndet,nstates,dim_psicoef
  integer(bit_kind), intent(in)  :: psidet(N_int,2,ndet)
  double precision, intent(in)   :: psicoef(dim_psicoef,nstates)
  integer, intent(in)            :: occpos(ndet)
  integer*8, allocatable         :: psi_det_save(:,:,:)
  double precision, allocatable  :: psi_coef_save(:,:)

  double precision               :: accu_norm
  integer                        :: i,j,k, ndet_qp_edit

  if (mpi_master) then
    ndet_qp_edit = min(ndet,N_det_qp_edit)

    call ezfio_set_determinants_N_int(N_int)
    call ezfio_set_determinants_bit_kind(bit_kind)
    call ezfio_set_dets_e_pos_N_e_pos_det(ndet)
    call ezfio_set_determinants_n_states(nstates)
    call ezfio_set_dets_e_pos_pos_occ(occpos)

    allocate (psi_det_save(N_int,2,ndet))
    do i=1,ndet
      do j=1,2
        do k=1,N_int
          psi_det_save(k,j,i) = transfer(psidet(k,j,i),1_8)
        enddo
      enddo
    enddo
    call ezfio_set_dets_e_pos_psi_e_pos_det(psi_det_save)
    deallocate (psi_det_save)

    allocate (psi_coef_save(ndet,nstates))
    do k=1,nstates
      do i=1,ndet
        psi_coef_save(i,k) = psicoef(i,k)
      enddo
      call normalize(psi_coef_save(1,k),ndet)
    enddo

    call ezfio_set_dets_e_pos_psi_e_pos_coef(psi_coef_save)
    deallocate (psi_coef_save)

    call write_int(6,ndet,'Saved positrons-electrons determinants')
  endif
end



subroutine save_ref_determinant_e_pos
  implicit none
  use bitmasks
  double precision               :: buffer(1,N_states)
  integer :: occpos
  occpos = 1
  buffer = 0.d0
  buffer(1,1) = 1.d0
  call save_wavefunction_general_e_pos(1,N_states,ref_bitmask,1,buffer,occpos)
end


subroutine convert_e_wf_to_e_pos_wf_with_thr(thr_wf)
  use bitmasks
  include 'constants.include.F'
 implicit none
  double precision, intent(in) :: thr_wf

  logical :: is_good_det
  integer, allocatable :: good_dets(:)
  integer, allocatable :: occpos(:)
  double precision, allocatable :: coefs(:,:)
  integer(bit_kind), allocatable :: psidet(:,:,:)
  integer :: i, n_good_dets
  allocate(good_dets(N_det))

  n_good_dets = 0
  do i = 1, n_det
   if(is_good_det(psi_det_sorted(1,1,i),psi_coef_sorted(i,1),thr_wf))then 
    n_good_dets += 1
    good_dets(n_good_dets) = i
   endif
  enddo

  allocate(occpos(n_good_dets),psidet(N_int,2,n_good_dets), coefs(n_good_dets,n_states))
  do i = 1, n_good_dets
   occpos(i) = 1
   psidet(:,:,i) = psi_det_sorted(:,:,good_dets(i))
   coefs(i,:)    = psi_coef_sorted(good_dets(i),:)
  enddo
  call save_wavefunction_general_e_pos(n_good_dets,n_states,psidet,n_good_dets,coefs,occpos)
end

logical function is_good_det(key_i, coef, thr)
 implicit none
  use bitmasks
 integer(bit_kind),intent(in)  :: key_i(N_int,2)
 double precision, intent(in)  :: coef(n_states), thr
 integer :: degree, i
 double precision :: accu
 call get_excitation_degree(HF_bitmask,key_i,degree,N_int) 
 if(degree.lt.2)then
  is_good_det = .True.
  return
 endif
!accu = 0.d0
!do i = 1, n_states
! accu += coef(i)**2
!enddo
!accu = dsqrt(accu)/dble(n_states)
 accu = coef(1) 
 is_good_det = accu > thr 
end
