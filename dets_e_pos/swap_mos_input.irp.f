program swap_mos
  implicit none
  BEGIN_DOC
  ! Swaps the indices of two molecular orbitals
  END_DOC
  integer                        :: i,j, i1, i2
  double precision               :: x
  integer :: index_mos_swap(2)
  print *,  'MOs to swap?'
  call ezfio_get_dets_e_pos_index_mos_swap(index_mos_swap)
  i1 = index_mos_swap(1)
  i2 = index_mos_swap(2)
  print*,'Swapping the following MOs : ',i1,i2
  do i=1,ao_num
    x = mo_coef(i,i1)
    mo_coef(i,i1) = mo_coef(i,i2)
    mo_coef(i,i2) = x
  enddo
  call save_mos
  
end
