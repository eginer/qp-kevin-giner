program test_rlm
 implicit none
 integer                       :: i,l,m
 double precision              :: radius,r(3),accu(2)
 double precision, allocatable :: rlm(:,:)
 external get_last_mo_pos_at_r
 read_wf = .True.
 touch read_wf
 l=angular_momentum
 allocate(rlm(2,-l:l))
 r=0.d0
 accu(1)=0.d0
 do i=1,npts_grid
   radius=dfloat(i)*step_grid
   r(1)=radius
   call get_rlm(radius,get_last_mo_pos_at_r,l,rlm)
  accu(2)=0.d0
  do m=-l,l
    accu(1)+=step_grid*radius**2*(rlm(1,m)**2.d0+rlm(2,m)**2.d0)
    accu(2)+=radius**2*(rlm(1,m)**2.d0+rlm(2,m)**2.d0)
  end do
  write(10,'(F13.6,E25.12)') radius,dsqrt(accu(2))
 end do
 deallocate(rlm)
 write(11,'(E25.12)') accu(1)
end program test_rlm

subroutine get_last_mo_pos_at_r(r,output)
 implicit none
 integer                       :: k
 double precision, intent(in)  :: r(3)
 double precision, intent(out) :: output
 double precision, allocatable :: mos_at_r(:)
 allocate(mos_at_r(mo_num))
 call give_all_mos_pos_at_r(r,mos_at_r) 
 k=angular_momentum+1
 !output=dot_product(rdm_pos_in_e_pos_eigvectors(:,mo_num,k),mos_at_r) 
 output=mos_at_r(k)
 deallocate(mos_at_r)
end subroutine get_last_mo_pos_at_r
