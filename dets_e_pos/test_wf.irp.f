program test_wf
 implicit none
 read_wf = .True.
 touch read_wf
 ! In Psi_det and psi_coef, there is the wave function stored in the EZFIO
! call test_sc2
! call routine
 call test_bla
end

subroutine test_bla
 implicit none
 print*,'n_s_beta_n_s_p  ',n_s_beta_n_s_p
 print*,'n_singles_beta  ',n_singles_beta
 print*,'n_single_e_plus ',n_single_e_plus
 print*,'n_s a * n_s ep  ',n_single_e_plus * n_singles_beta

 print*,'n_s_alpha_n_s_p ',n_s_alpha_n_s_p
 print*,'n_singles_alpha ',n_singles_alpha
 print*,'n_single_e_plus ',n_single_e_plus
 print*,'n_s a * n_s ep  ',n_single_e_plus * n_singles_alpha


end

subroutine routine
 implicit none
  use bitmasks
 integer :: i,j
 integer :: i_pos, j_pos
! print*,'mo_grad_ints'
! do i = 1, mo_num
!  write(*,'(100(F16.10,X))')mo_grad_ints(i,:,1)
! enddo
 i_pos = 1
 j_pos = 1
 double precision :: accu, norm, o1_ij, o2_ij,hij,accu2,o2_ep
 double precision, allocatable :: ints_pos_ij(:,:), ints_e_ij(:,:)
 double precision, allocatable :: ints_e_pos(:,:,:,:)
 allocate(ints_pos_ij(mo_num, mo_num), ints_e_ij(mo_num, mo_num))
 allocate(ints_e_pos(mo_num, mo_num, mo_num, mo_num))
 ints_e_ij = mo_one_e_integrals 
 ints_e_pos = 0.d0
 ints_pos_ij = 0.d0
 accu = 0.d0
 accu2 = 0.d0
 norm = 0.d0
 do i = 1, N_det
  norm += psi_coef(i,1) * psi_coef(i,1)
  do j = 1, N_det
   call i_o1_o2_j(psi_det(1,1,i), psi_det(1,1,j), i_pos, j_pos, &
     ints_e_ij, ints_pos_ij, two_e_ints_array, ints_e_pos, N_int, o1_ij, o2_ij, o2_ep)

   call i_H_j(psi_det(1,1,i), psi_det(1,1,j), N_int, hij)
!   print*,hij,(o1_ij+ o2_ij)
!   if(dabs(hij -  (o1_ij+ o2_ij)).gt.1.d-10)then
!    print*,'i,j',i,j
!    print*,hij,(o1_ij+ o2_ij),dabs(hij -  (o1_ij+ o2_ij))
!    stop
!   endif
   accu += ( o1_ij + o2_ij) * psi_coef(i,1) * psi_coef(j,1)
   accu2 += ( hij) * psi_coef(i,1) * psi_coef(j,1)
   print*,hij,psi_coef(i,1),psi_coef(j,1)
  enddo
 enddo
 norm = dsqrt(norm)
 print*,'accu = ',accu/norm
 print*,'accu2= ',accu2/norm
! call print_energy_components
end

subroutine test_sc2
 implicit none
 integer :: i,j,k,l,degree,i_double
 double precision :: hij
 double precision, allocatable :: delta_h(:), u_in(:)
 double precision, allocatable :: hmatrix(:,:), eigenvectors(:,:), eigenvalues(:)

 print*,'N_det = ',N_det
 allocate(delta_h(N_det),u_in(N_det),hmatrix(N_det,N_det),eigenvectors(N_det,N_det),eigenvalues(N_det))
 do i = 1, N_det
  u_in(i) = psi_coef(i,1)
!  print*,'u_in(i) = ',u_in(i) 
 enddo

 call get_sc2_dressing(u_in, delta_h)
 stop
 do i = 1, N_det
  do j = 1, N_det
   hmatrix(j,i) = H_matrix_all_dets(j,i)
  enddo
 enddo
 do i = 1, N_det
  hmatrix(i,i) += delta_h(i)
 enddo
 call lapack_diag(eigenvalues,eigenvectors,hmatrix,N_det,N_det)
end

subroutine get_sc2_dressing(u_in, delta_h)
 implicit none
  BEGIN_DOC
! dressing of the CISD matrix based on the knowledge that determinants are stored in psi_det 
! and produces the diagonal dressing
  END_DOC
 double precision, intent(in)  :: u_in(N_det)
 double precision, intent(out) :: delta_h(N_det)  
 integer :: i,j,k,l,degree,i_double
 double precision :: hij
 double precision, allocatable :: ecorr(:)  
 delta_h = 0.d0
 allocate(ecorr(n_doubles_in_psi_det))
 double precision :: accu
 accu = 0.d0
 do j = 1, n_doubles_in_psi_det
  i_double = list_doubles(j)
  call i_H_j(HF_bitmask,psi_det(1,1,i_double),N_int,hij)
  ecorr(j) = hij * u_in(i_double) / u_in(index_hf) 
  accu += ecorr(j)
 enddo
 print*,'accu = ',accu
 do i = 2, N_det
  do j = 1, n_doubles_in_psi_det
   i_double = list_doubles(j)
   call get_excitation_degree(psi_det(1,1,i),psi_det(1,1,i_double),degree,N_int)
   if(degree>2)then
    delta_h(i) += ecorr(j)
   endif
  enddo
 enddo

end
