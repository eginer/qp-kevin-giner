
  use bitmasks ! you need to include the bitmasks_module.f90 features
 BEGIN_PROVIDER [ integer, list_larger_dets_wf, (n_states)]
&BEGIN_PROVIDER [double precision , largest_coef_n_states , (n_states)]
 implicit none
 BEGIN_DOC
 ! index of the largest Slater determinant (in absolute value) for each state
 END_DOC
 integer :: i,j
 integer, allocatable :: iorder(:)
 double precision, allocatable :: coef(:)
 allocate(coef(n_e_pos_det),iorder(n_e_pos_det))
 do i = 1, n_states
  do j = 1, n_e_pos_det
   iorder(j) = j
   coef(j) = -dabs(psi_e_pos_coef(j,i))
  enddo
  call dsort(coef, iorder, n_e_pos_det)
  list_larger_dets_wf(i) = iorder(1)
  print*,'largest coef = ',coef(1),iorder(1)
  largest_coef_n_states(j) = dabs(coef(1))
 enddo
END_PROVIDER 

 BEGIN_PROVIDER [integer(bit_kind), largest_e_det_n_states , (2,N_int,n_states)]
&BEGIN_PROVIDER [integer          , largest_pos_det_n_states , (n_states)]
  use bitmasks ! you need to include the bitmasks_module.f90 features
 implicit none
 BEGIN_DOC
 ! Dominant electron Slater determinant and positron occupation for each state
 END_DOC
 integer :: i
 do i = 1, n_states
  largest_e_det_n_states(1:2,1:N_int,i) = psi_e_pos_det(1:2,1:N_int,list_larger_dets_wf(i))
  largest_pos_det_n_states(i)=pos_occ(list_larger_dets_wf(i))
 enddo
 
END_PROVIDER 

subroutine print_wf_exc
 implicit none
 integer :: i ,j 
  print*,'********'
  do i=1,N_states
   print*,'--------------'
   print*,'State = ',i
   print*,'Energy = ',cisd_e_pos_energies(i)
   print*,'Position of the largest coef = ',list_larger_dets_wf(i)
   print*,'Largest electronic Slater determinant '
   call debug_det(largest_e_det_n_states(1,1,i),N_int)
   print*,'Positron occupancy  = ',largest_pos_det_n_states(i)
   print*,'--------------'
  enddo

end

