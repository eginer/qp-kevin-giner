
BEGIN_PROVIDER [ double precision, Fock_matrix_mo_w_pos_alpha, (mo_num,mo_num) ]
   implicit none
   BEGIN_DOC
   ! Fock matrix for ALPHA electrons on the MO basis with e-positron interaction
   END_DOC
   call ao_to_mo(Fock_matrix_ao_w_pos_alpha,size(Fock_matrix_ao_w_pos_alpha,1), &
                 Fock_matrix_mo_w_pos_alpha,size(Fock_matrix_mo_w_pos_alpha,1))
END_PROVIDER


BEGIN_PROVIDER [ double precision, Fock_matrix_mo_w_pos_beta, (mo_num,mo_num) ]
   implicit none
   BEGIN_DOC
   ! Fock matrix for BETA electrons on the MO basis with e-positron interaction
   END_DOC
   call ao_to_mo(Fock_matrix_ao_w_pos_beta,size(Fock_matrix_ao_w_pos_beta,1), &
                 Fock_matrix_mo_w_pos_beta,size(Fock_matrix_mo_w_pos_beta,1))
END_PROVIDER


 BEGIN_PROVIDER [ double precision, Fock_matrix_ao_w_pos_alpha, (ao_num, ao_num) ]
&BEGIN_PROVIDER [ double precision, Fock_matrix_ao_w_pos_beta,  (ao_num, ao_num) ]
 implicit none
 BEGIN_DOC
   ! Fock matrix for ALPHA electrons on the AO basis with e-positron interaction
   !
   ! Fock matrix for BETA electrons on the AO basis with e-positron interaction
 END_DOC

 integer                        :: i,j
 do j=1,ao_num
   do i=1,ao_num
     Fock_matrix_ao_w_pos_alpha(i,j) = ao_one_e_integrals(i,j) + ao_two_e_integral_alpha_w_pos(i,j)
     Fock_matrix_ao_w_pos_beta (i,j) = ao_one_e_integrals(i,j) + ao_two_e_integral_beta_w_pos (i,j)
   enddo
 enddo

END_PROVIDER

 BEGIN_PROVIDER [ double precision, Fock_matrix_mo_e_w_pos, (mo_num,mo_num) ]
&BEGIN_PROVIDER [ double precision, Fock_matrix_diag_mo_e_w_pos, (mo_num)]
   implicit none
   BEGIN_DOC
   ! Fock matrix on the MO basis for electron including the e-positron interaction
   !
   ! For open shells, the ROHF Fock Matrix is ::
   !
   !       |   F-K    |  F + K/2  |    F     |
   !       |---------------------------------|
   !       | F + K/2  |     F     |  F - K/2 |
   !       |---------------------------------|
   !       |    F     |  F - K/2  |  F + K   |
   !
   !
   ! F = 1/2 (Fa + Fb)
   !
   ! K = Fb - Fa
   !
   END_DOC
   integer                        :: i,j,n
   if (elec_alpha_num == elec_beta_num) then
     Fock_matrix_mo_e_w_pos = Fock_matrix_mo_w_pos_alpha
   else

     do j=1,elec_beta_num
       ! F-K
       do i=1,elec_beta_num !CC
         Fock_matrix_mo_e_w_pos(i,j) = 0.5d0*(Fock_matrix_mo_w_pos_alpha(i,j)+Fock_matrix_mo_w_pos_beta(i,j))&
             - (Fock_matrix_mo_w_pos_beta(i,j) - Fock_matrix_mo_w_pos_alpha(i,j))
       enddo
       ! F+K/2
       do i=elec_beta_num+1,elec_alpha_num  !CA
         Fock_matrix_mo_e_w_pos(i,j) = 0.5d0*(Fock_matrix_mo_w_pos_alpha(i,j)+Fock_matrix_mo_w_pos_beta(i,j))&
             + 0.5d0*(Fock_matrix_mo_w_pos_beta(i,j) - Fock_matrix_mo_w_pos_alpha(i,j))
       enddo
       ! F
       do i=elec_alpha_num+1, mo_num !CV
         Fock_matrix_mo_e_w_pos(i,j) = 0.5d0*(Fock_matrix_mo_w_pos_alpha(i,j)+Fock_matrix_mo_w_pos_beta(i,j))
       enddo
     enddo

     do j=elec_beta_num+1,elec_alpha_num
       ! F+K/2
       do i=1,elec_beta_num !AC
         Fock_matrix_mo_e_w_pos(i,j) = 0.5d0*(Fock_matrix_mo_w_pos_alpha(i,j)+Fock_matrix_mo_w_pos_beta(i,j))&
             + 0.5d0*(Fock_matrix_mo_w_pos_beta(i,j) - Fock_matrix_mo_w_pos_alpha(i,j))
       enddo
       ! F
       do i=elec_beta_num+1,elec_alpha_num !AA
         Fock_matrix_mo_e_w_pos(i,j) = 0.5d0*(Fock_matrix_mo_w_pos_alpha(i,j)+Fock_matrix_mo_w_pos_beta(i,j))
       enddo
       ! F-K/2
       do i=elec_alpha_num+1, mo_num !AV
         Fock_matrix_mo_e_w_pos(i,j) = 0.5d0*(Fock_matrix_mo_w_pos_alpha(i,j)+Fock_matrix_mo_w_pos_beta(i,j))&
             - 0.5d0*(Fock_matrix_mo_w_pos_beta(i,j) - Fock_matrix_mo_w_pos_alpha(i,j))
       enddo
     enddo

     do j=elec_alpha_num+1, mo_num
       ! F
       do i=1,elec_beta_num !VC
         Fock_matrix_mo_e_w_pos(i,j) = 0.5d0*(Fock_matrix_mo_w_pos_alpha(i,j)+Fock_matrix_mo_w_pos_beta(i,j))
       enddo
       ! F-K/2
       do i=elec_beta_num+1,elec_alpha_num !VA
         Fock_matrix_mo_e_w_pos(i,j) = 0.5d0*(Fock_matrix_mo_w_pos_alpha(i,j)+Fock_matrix_mo_w_pos_beta(i,j))&
             - 0.5d0*(Fock_matrix_mo_w_pos_beta(i,j) - Fock_matrix_mo_w_pos_alpha(i,j))
       enddo
       ! F+K
       do i=elec_alpha_num+1,mo_num !VV
         Fock_matrix_mo_e_w_pos(i,j) = 0.5d0*(Fock_matrix_mo_w_pos_alpha(i,j)+Fock_matrix_mo_w_pos_beta(i,j)) &
             + (Fock_matrix_mo_w_pos_beta(i,j) - Fock_matrix_mo_w_pos_alpha(i,j))
       enddo
     enddo

   endif

   do i = 1, mo_num
     Fock_matrix_diag_mo_e_w_pos(i) = Fock_matrix_mo_e_w_pos(i,i)
   enddo


   if(frozen_orb_scf)then
     integer                        :: iorb,jorb
     do i = 1, n_core_orb
      iorb = list_core(i)
      do j = 1, n_act_orb
       jorb = list_act(j)
       Fock_matrix_mo_e_w_pos(iorb,jorb) = 0.d0
       Fock_matrix_mo_e_w_pos(jorb,iorb) = 0.d0
      enddo
     enddo
   endif

   if(no_oa_or_av_opt)then
     do i = 1, n_act_orb
       iorb = list_act(i)
       do j = 1, n_inact_orb
         jorb = list_inact(j)
         Fock_matrix_mo_e_w_pos(iorb,jorb) = 0.d0
         Fock_matrix_mo_e_w_pos(jorb,iorb) = 0.d0
       enddo
       do j = 1, n_virt_orb
         jorb = list_virt(j)
         Fock_matrix_mo_e_w_pos(iorb,jorb) = 0.d0
         Fock_matrix_mo_e_w_pos(jorb,iorb) = 0.d0
       enddo
       do j = 1, n_core_orb
         jorb = list_core(j)
         Fock_matrix_mo_e_w_pos(iorb,jorb) = 0.d0
         Fock_matrix_mo_e_w_pos(jorb,iorb) = 0.d0                                                                                                                 
       enddo
     enddo
   endif

END_PROVIDER


