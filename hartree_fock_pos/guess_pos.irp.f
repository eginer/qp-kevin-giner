subroutine create_guess_elec
  implicit none
  BEGIN_DOC
!   Create a MO guess for electrons if no MOs are present in the EZFIO directory 
  END_DOC
  logical                        :: exists
  PROVIDE ezfio_filename
  call ezfio_has_mo_basis_mo_coef(exists)
  if (.not.exists) then
    print*,'No electronic MOs in EZFIO, so I am creating a guess for electronic MOs '
    mo_label = 'Guess'
    print*,'Guess = ',mo_guess_type
    if (mo_guess_type == "HCore") then
      mo_coef = ao_ortho_lowdin_coef
      call restore_symmetry(ao_num,mo_num,mo_coef,size(mo_coef,1),1.d-10)
      TOUCH mo_coef
      call mo_as_eigvectors_of_mo_matrix(mo_one_e_integrals,     &
          size(mo_one_e_integrals,1),                            &
          size(mo_one_e_integrals,2),                            &
          mo_label,1,.false.)
      call restore_symmetry(ao_num,mo_num,mo_coef,size(mo_coef,1),1.d-10)
      SOFT_TOUCH mo_coef
    else if (mo_guess_type == "Huckel") then
      call huckel_guess
    else
      print *,  'Unrecognized MO guess type : '//mo_guess_type
      stop 1
    endif
    SOFT_TOUCH mo_label
  endif
end


subroutine create_guess_pos
  implicit none
  BEGIN_DOC
!   Create a MO guess for positrons if no MOs are present in the EZFIO directory 
  END_DOC
  logical                        :: exists
  PROVIDE ezfio_filename
  call ezfio_has_positon_mos_mo_coef_pos(exists)
  if (.not.exists) then
   print*,'No POSITRONIC MOs in EZFIO, so I am creating a guess for electronic MOs '
   call hcore_guess_pos
   SOFT_TOUCH mo_coef_pos
  endif
end



subroutine hcore_guess_pos
 implicit none
 BEGIN_DOC
 ! Set the mo_coef_pos coefficients to the eigenvector of the one-body integrals for positrons 
 END_DOC
      mo_coef_pos = ao_ortho_lowdin_coef
      call restore_symmetry(ao_num,mo_num,mo_coef_pos,size(mo_coef_pos,1),1.d-10)
      TOUCH mo_coef_pos
      call mo_pos_as_eigvectors_of_mo_matrix(mo_pos_integrals,     &
          size(mo_pos_integrals,1),                            &
          size(mo_pos_integrals,2),                            &
          mo_label,1,.True.)
      call restore_symmetry(ao_num,mo_num,mo_coef_pos,size(mo_coef_pos,1),1.d-10)
      SOFT_TOUCH mo_coef_pos

end

subroutine mo_pos_as_eigvectors_of_mo_matrix(matrix,n,m,label,sign,output)
 BEGIN_DOC
 ! Set the mo_coef_pos coefficients as eigenvectors of a given one-body matrix for positrons 
 END_DOC
  implicit none
  integer,intent(in)             :: n,m, sign
  character*(64), intent(in)     :: label
  double precision, intent(in)   :: matrix(n,m)
  logical, intent(in)            :: output

  integer :: i,j
  double precision, allocatable  :: mo_coef_pos_new(:,:), R(:,:),eigvalues(:), A(:,:)
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: mo_coef_pos_new, R

  call write_time(6)
  if (m /= mo_num) then
    print *, irp_here, ': Error : m/= mo_num'
    stop 1
  endif
  allocate(A(n,m),R(n,m),mo_coef_pos_new(ao_num,m),eigvalues(m))
  if (sign == -1) then
    do j=1,m
      do i=1,n
        A(i,j) = -matrix(i,j)
      enddo
    enddo
  else
    do j=1,m
      do i=1,n
        A(i,j) = matrix(i,j)
      enddo
    enddo
  endif
  mo_coef_pos_new = mo_coef_pos

  call lapack_diag(eigvalues,R,A,n,m)
  if (output) then
    write (6,'(A)')  'Positronic MOs are now **'//trim(label)//'**'
    write (6,'(A)') ''
    write (6,'(A)')  'Eigenvalues for positronic MOs'
    write (6,'(A)') '-----------'
    write (6,'(A)')  ''
    write (6,'(A)') '======== ================'
  endif
  if (sign == -1) then
    do i=1,m
      eigvalues(i) = -eigvalues(i)
    enddo
  endif
  if (output) then
    do i=1,m
      write (6,'(I8,1X,F16.10)')  i,eigvalues(i)
    enddo
    write (6,'(A)') '======== ================'
    write (6,'(A)')  ''
  endif

  call dgemm('N','N',ao_num,m,m,1.d0,mo_coef_pos_new,size(mo_coef_pos_new,1),R,size(R,1),0.d0,mo_coef_pos,size(mo_coef_pos,1))
  deallocate(A,mo_coef_pos_new,R,eigvalues)
  call write_time(6)

end
