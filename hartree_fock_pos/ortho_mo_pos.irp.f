subroutine orthonormalize_mo_pos
  implicit none
  integer :: m,p
  m = size(mo_coef_pos,1)
  p = size(mo_pos_overlap,1)
  call ortho_lowdin(mo_pos_overlap,p,mo_num,mo_coef_pos,m,ao_num,lin_dep_cutoff)
  SOFT_TOUCH mo_coef_pos
end

