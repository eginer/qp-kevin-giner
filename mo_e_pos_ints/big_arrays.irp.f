BEGIN_PROVIDER [ double precision, two_e_ints_array, (mo_num, mo_num, mo_num, mo_num)]
 implicit none
 double precision :: get_two_e_integral
  PROVIDE mo_two_e_integrals_in_map mo_integrals_cache
 integer :: i,j,k,l
 do l = 1, mo_num
  do k = 1, mo_num
   do j = 1, mo_num
    do i = 1, mo_num
     two_e_ints_array(i,j,k,l) =  get_two_e_integral(i,j,k,l,mo_integrals_map)
    enddo
   enddo
  enddo
 enddo
END_PROVIDER 

