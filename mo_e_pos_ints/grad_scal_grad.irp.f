BEGIN_PROVIDER [ double precision, grad_scal_ints, (mo_num, mo_num, mo_num, mo_num) ]
 implicit none
  BEGIN_DOC
! Dot product of the impulsion of electrons to be used in the kinetic energy of the center of mass of two electrons 
! \frac{1}{2}(p_1^2 + p_2^2) = P^2 + R^2/4, where P=\frac{1}{2}(p_1 + p_2), R = p_1 - p_2. 
!
! P^2 = \frac{1}{4}(p_1^2 + p_2^2) + \frac{1}{4}(p_1 .dot. p_2)
!
! Here wd compute the integral of 
! 
! \frac{1}{4}(p_1 .dot. p_2) = \frac{1}{2}\sum_{i.ne.} -1/2 {(d/dx1 d/dx2) + d/dx2 d/dx1}
!
! grad_scal_ints(i,j,k,m) = <ij | \frac{1}{2}(p_1 .dot. p_2) | kl>
!                         = -\frac{1}{2} (<i|d/dx1|k> <j|d/dx2|l> + <i|d/dx1|k> <j|d/dx2|l> ) 
!                         = - <i|d/dx|k> <j|d/dx|l> 
  END_DOC
 integer :: i,j,k,l,m
 grad_scal_ints = 0.d0
 do l = 1, mo_num
  do k = 1, mo_num
   do j = 1, mo_num
    do i = 1, mo_num
     do m = 1, 3
      grad_scal_ints(i,j,k,l) += -(mo_grad_ints_transp(m,i,k) * mo_grad_ints_transp(m,j,l)) * 0.5d0
     enddo
    enddo
   enddo
  enddo
 enddo
END_PROVIDER 

BEGIN_PROVIDER [ double precision, e_pos_grad_scal_ints, (mo_num, mo_num, mo_num, mo_num) ]
 implicit none
  BEGIN_DOC
! Dot product of the impulsion of electrons and positrons to be used in the kinetic energy of the center of mass the system electron/positron 
! \frac{1}{2}(p_1^2 + p_2^2) = P^2 + R^2/4, where P=\frac{1}{2}(p_1 + p_2), R = p_1 - p_2. 
!
! P^2 = \frac{1}{4}(p_1^2 + p_2^2) + \frac{1}{4}(p_1 .dot. p_2 + p_2 .dot. p_2)
!
! Here wd compute the integral of 
! 
! \frac{1}{4}(p_1 .dot. p_2 + p_1 .dot. p_2) = \frac{1}{2}\sum_{i.ne.} -1/2 {(d/dx1 d/dx2) + d/dx2 d/dx1}
!
! e_pos_grad_scal_ints(i,j,k,m) = <i_elec j_pos | \frac{1}{2}(p_1 .dot. p_2 + p_2 .dot. p_1) | k_elec l_pos>
!                         = -\frac{1}{2} (<i|d/dx1|k> <j|d/dx2|l> + <i|d/dx1|k> <j|d/dx2|l> ) 
!                         = - <i|d/dx|k> <j|d/dx|l> 
!
! WARNING ::: NOT SYMMETRICAL ::: because (i,k) ARE ELECTRONS and (j,l) ARE POSITRONS 
  END_DOC
 integer :: i,j,k,l,m
 e_pos_grad_scal_ints = 0.d0
 do l = 1, mo_num
  do k = 1, mo_num
   do j = 1, mo_num
    do i = 1, mo_num
     do m = 1, 3
      e_pos_grad_scal_ints(i,j,k,l) += -(mo_grad_ints_transp(m,i,k) * mo_pos_grad_ints_transp(m,j,l)) * 0.5d0
     enddo
    enddo
   enddo
  enddo
 enddo
END_PROVIDER 

BEGIN_PROVIDER [ double precision, minus_e_pos_grad_scal_ints, (mo_num, mo_num, mo_num, mo_num) ]
 implicit none
  BEGIN_DOC
! Dot product of the impulsion of electrons and positrons to be used in the kinetic energy of the center of mass the system electron/positron 
! \frac{1}{2}(p_1^2 + p_2^2) = P^2 + R^2/4, where P=\frac{1}{2}(p_1 + p_2), R = p_1 - p_2. 
!
! P^2 = \frac{1}{4}(p_1^2 + p_2^2) + \frac{1}{4}(p_1 .dot. p_2 + p_2 .dot. p_2)
!
! Here wd compute the integral of 
! 
! \frac{1}{4}(p_1 .dot. p_2 + p_1 .dot. p_2) = \frac{1}{2}\sum_{i.ne.} -1/2 {(d/dx1 d/dx2) + d/dx2 d/dx1}
!
! e_pos_grad_scal_ints(i,j,k,m) = <i_elec j_pos | \frac{1}{2}(p_1 .dot. p_2 + p_2 .dot. p_1) | k_elec l_pos>
!                         = -\frac{1}{2} (<i|d/dx1|k> <j|d/dx2|l> + <i|d/dx1|k> <j|d/dx2|l> ) 
!                         = - <i|d/dx|k> <j|d/dx|l> 
!
! WARNING ::: NOT SYMMETRICAL ::: because (i,k) ARE ELECTRONS and (j,l) ARE POSITRONS 
  END_DOC
 integer :: i,j,k,l,m
 minus_e_pos_grad_scal_ints = 0.d0
 do l = 1, mo_num
  do k = 1, mo_num
   do j = 1, mo_num
    do i = 1, mo_num
     do m = 1, 3
      minus_e_pos_grad_scal_ints(i,j,k,l) += (mo_grad_ints_transp(m,i,k) * mo_pos_grad_ints_transp(m,j,l))
     enddo
    enddo
   enddo
  enddo
 enddo
END_PROVIDER 


 BEGIN_PROVIDER [ double precision, mo_pos_grad_ints, (mo_num, mo_num,3)]
 implicit none
 BEGIN_DOC
! mo_grad_ints(i,j,m) = <phi_i^MO | d/dx | phi_j^MO> WITH MOS OF THE POSITRON 
 END_DOC
 integer :: i,j,ipoint,m
 double precision :: weight
 mo_pos_grad_ints = 0.d0
 do m = 1, 3
  do ipoint = 1, n_points_final_grid
   weight = final_weight_at_r_vector(ipoint)
   do j = 1, mo_num
    do i = 1, mo_num
     mo_pos_grad_ints(i,j,m) += mos_pos_grad_in_r_array(j,ipoint,m) * mos_pos_in_r_array(i,ipoint) * weight
    enddo
   enddo
  enddo
 enddo


END_PROVIDER 

 BEGIN_PROVIDER [ double precision, mo_pos_grad_ints_transp, (3,mo_num, mo_num)]
 implicit none
 BEGIN_DOC
! mo_pos_grad_ints(i,j,m) = <phi_i^MO | d/dx | phi_j^MO>
 END_DOC
 integer :: i,j,ipoint,m
 double precision :: weight
 do m = 1, 3
  do j = 1, mo_num
   do i = 1, mo_num
    mo_pos_grad_ints_transp(m,i,j) = mo_pos_grad_ints(i,j,m)
   enddo
  enddo
 enddo


END_PROVIDER 

! BEGIN_PROVIDER [ double precision, mo_pos_grad_ints, (mo_num, mo_num,3)]
! implicit none
! BEGIN_DOC
! mo_grad_ints(i,j,m) = <phi_i^MO | d/dx | phi_j^MO> WITH MOS OF THE POSITRON 
! END_DOC
! integer :: i,j,ipoint,m
! double precision :: weight
! mo_pos_grad_ints = 0.d0
! do m = 1, 3
!  do ipoint = 1, n_points_final_grid
!   weight = final_weight_at_r_vector(ipoint)
!   do j = 1, mo_num
!    do i = 1, mo_num
!     mo_pos_grad_ints(i,j,m) += mos_pos_grad_in_r_array(j,ipoint,m) * mos_pos_in_r_array(i,ipoint) * weight
!    enddo
!   enddo
!  enddo
! enddo
!
!
!END_PROVIDER 
!
! BEGIN_PROVIDER [ double precision, mo_pos_grad_ints_transp, (3,mo_num, mo_num)]
! implicit none
! BEGIN_DOC
! mo_pos_grad_ints(i,j,m) = <phi_i^MO | d/dx | phi_j^MO>
! END_DOC
! integer :: i,j,ipoint,m
! double precision :: weight
! do m = 1, 3
!  do j = 1, mo_num
!   do i = 1, mo_num
!    mo_pos_grad_ints_transp(m,i,j) = mo_pos_grad_ints(i,j,m)
!   enddo
!  enddo
! enddo
!
!
!END_PROVIDER 
