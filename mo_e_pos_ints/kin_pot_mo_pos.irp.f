
BEGIN_PROVIDER [double precision, mo_pos_kinetic_integrals, (mo_num,mo_num)]
  implicit none
  BEGIN_DOC
  !  Kinetic energy integrals in the MO basis of the positron 
  END_DOC
  call ao_to_mo_pos(ao_kinetic_integrals,size(ao_kinetic_integrals,1),& 
                    mo_pos_kinetic_integrals,size(mo_pos_kinetic_integrals,1))

END_PROVIDER


BEGIN_PROVIDER [double precision, mo_pos_nuclear_integrals, (mo_num,mo_num)]
  implicit none
  BEGIN_DOC
  !  positron - nuclear energy integrals in the MO basis of the positron 
  END_DOC
  call ao_to_mo_pos(ao_integrals_n_e,size(ao_integrals_n_e,1),& 
                    mo_pos_nuclear_integrals,size(mo_pos_nuclear_integrals,1))
  mo_pos_nuclear_integrals = -1.d0 * mo_pos_nuclear_integrals
END_PROVIDER

