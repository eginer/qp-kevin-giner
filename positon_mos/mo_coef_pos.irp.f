
BEGIN_PROVIDER [ double precision, mo_coef_pos, (ao_num,mo_num) ]
  implicit none
  BEGIN_DOC
  ! Molecular orbital coefficients on |AO| basis set FOR THE POSITRONS 
  !
  ! mo_coef_pos(i,j) = coefficient of the i-th |AO| on the jth |MO|
  !
  END_DOC
  integer                        :: i, j
  double precision, allocatable  :: buffer(:,:)
  logical                        :: exists
  PROVIDE ezfio_filename


  if (mpi_master) then
    ! Coefs
    call ezfio_has_positon_mos_mo_coef_pos(exists)
  endif
  IRP_IF MPI_DEBUG
    print *,  irp_here, mpi_rank
    call MPI_BARRIER(MPI_COMM_WORLD, ierr)
  IRP_ENDIF
  IRP_IF MPI
    include 'mpif.h'
    integer :: ierr
    call MPI_BCAST(exists, 1, MPI_LOGICAL, 0, MPI_COMM_WORLD, ierr)
    if (ierr /= MPI_SUCCESS) then
      stop 'Unable to read mo_coef_pos with MPI'
    endif
  IRP_ENDIF

  if (exists) then
    if (mpi_master) then
      call ezfio_get_positon_mos_mo_coef_pos(mo_coef_pos)
      write(*,*) 'Read  mo_coef_pos'
    endif
    IRP_IF MPI
      call MPI_BCAST( mo_coef_pos, mo_num*ao_num, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      if (ierr /= MPI_SUCCESS) then
        stop 'Unable to read mo_coef_pos with MPI'
      endif
    IRP_ENDIF
  else
    ! Orthonormalized AO basis
    do i=1,mo_num
      do j=1,ao_num
        mo_coef_pos(j,i) = ao_ortho_canonical_coef(j,i)
      enddo
    enddo
  endif
END_PROVIDER

subroutine ao_to_mo_pos(A_ao,LDA_ao,A_mo_pos,LDA_mo_pos)
  implicit none
  BEGIN_DOC
  ! Transform A from the |AO| basis to the |mo_pos| basis
  !
  ! $C^\dagger.A_{ao}.C$
  END_DOC
  integer, intent(in)            :: LDA_ao,LDA_mo_pos
  double precision, intent(in)   :: A_ao(LDA_ao,ao_num)
  double precision, intent(out)  :: A_mo_pos(LDA_mo_pos,mo_num)
  double precision, allocatable  :: T(:,:)

  allocate ( T(ao_num,mo_num) )
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: T

  call dgemm('N','N', ao_num, mo_num, ao_num,                    &
      1.d0, A_ao,LDA_ao,                                             &
      mo_coef_pos, size(mo_coef_pos,1),                                      &
      0.d0, T, size(T,1))

  call dgemm('T','N', mo_num, mo_num, ao_num,                &
      1.d0, mo_coef_pos,size(mo_coef_pos,1),                                 &
      T, ao_num,                                                     &
      0.d0, A_mo_pos, size(A_mo_pos,1))

  call restore_symmetry(mo_num,mo_num,A_mo_pos,size(A_mo_pos,1),1.d-12)
  deallocate(T)
end                                              


BEGIN_PROVIDER [ double precision, mo_coef_pos_transp, (mo_num,ao_num) ]                                                                         
  implicit none
  BEGIN_DOC
  ! |MO| coefficients on |AO| basis set FOR THE POSITRONS 
  END_DOC
  integer                        :: i, j

  do j=1,ao_num
    do i=1,mo_num
      mo_coef_pos_transp(i,j) = mo_coef_pos(j,i)
    enddo
  enddo

END_PROVIDER

